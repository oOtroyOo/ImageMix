﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageMix
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

        }



        void TextBoxInChanged(TextBox box, PictureBox pictureBox)
        {
            if (File.Exists(box.Text))
            {
                LoadImage(box.Text, pictureBox);
            }
        }

        void LoadImage(string filename, PictureBox pictureBox)
        {
            //Image image = Image.FromFile(filename);
            //pictureBox.Image = image;
            pictureBox.LoadAsync(filename);

        }

        void DoPic()
        {
            Image image1 = pictureBox1.Image;
            Image image2 = pictureBox2.Image;
            if (image1 != null && image1 != pictureBox1.ErrorImage &&
                image2 != null && image2 != pictureBox2.ErrorImage)
            {
                Program.mixer.Image1 = image1;
                Program.mixer.Image2 = image2;

                if (Program.form2 == null || Program.form2.IsDisposed)
                {
                    Program.form2 = new Form2();
                }
                Program.form2.Show();
            }
        }


        #region events



        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

            pictureBox1.LoadAsync(Program.DialogToText(sender as FileDialog));
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            pictureBox2.LoadAsync(Program.DialogToText(sender as FileDialog));
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            TextBoxInChanged((TextBox)sender, pictureBox2);
        }
        private void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            DoPic();
        }
        private void pictureBox2_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            DoPic();
        }
        #endregion

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void radioButton_Click(object sender, EventArgs e)
        {
            Program.mixer.colorType = (Mixer.ColorType)groupBox1.Controls.OfType<RadioButton>().ToList().IndexOf(sender as RadioButton);
            Console.WriteLine(Program.mixer.colorType);
            DoPic();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (RadioButton control in groupBox1.Controls.OfType<RadioButton>())
            {
                control.Click += radioButton_Click;
            }
        }
    }
}
