﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageMix
{
    public partial class Form2 : Form
    {
        private string textBoxSave;
        public Form2()
        {
            InitializeComponent();
        }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBoxSave = Program.DialogToText((FileDialog)sender);
            Program.form2.pictureBox3.Image.Save(textBoxSave,ImageFormat.Png);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            Program.from1.Enabled = false;
            buttonSave.Enabled = false;
            timer1.Enabled = true;
            timer1.Start();

        }

        private void Form2_Closed(object sender, EventArgs e)
        {
            Program.from1.Enabled = true;
            Program.mixer.Dispose();
        }
        public void SetImage(Image newImage, PictureBox pictureBox)
        {
            if (newImage == null)
            {
                newImage = pictureBox.ErrorImage;
            }
            else
            {
                buttonSave.Enabled = true;
            }
            pictureBox.Image = newImage;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.White;

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            BackColor = Color.Black;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;
            Program.mixer.DoImage(img =>
            {
                SetImage(img, Program.form2.pictureBox3);
            });
        }
    }
}
