﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace ImageMix
{
    public class ImgHelper
    {  ///
       /// Resize图片
       ///
       /// 原始Bitmap
       /// 新的宽度
       /// 新的高度
       /// 保留着，暂时未用
       /// 处理以后的图片
        public static Bitmap KiResizeImage(Bitmap bmp, int newW, int newH, int Mode)
        {
            try
            {
                Bitmap b = new Bitmap(newW, newH);
                Graphics g = Graphics.FromImage(b);

                // 插值算法的质量
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(bmp, new Rectangle(0, 0, newW, newH), new Rectangle(0, 0, bmp.Width, bmp.Height), GraphicsUnit.Pixel);
                g.Dispose();

                return b;
            }
            catch
            {
                return null;
            }
        }



        // ===============================

        ///
        /// 剪裁 -- 用GDI+
        ///
        /// 原始Bitmap
        /// 开始坐标X
        /// 开始坐标Y
        /// 宽度
        /// 高度
        /// 剪裁后的Bitmap
        public static Bitmap KiCut(Bitmap b, int StartX, int StartY, int iWidth, int iHeight)
        {
            if (b == null)
            {
                return null;
            }

            int w = b.Width;
            int h = b.Height;

            if (StartX >= w || StartY >= h)
            {
                return null;
            }

            if (StartX + iWidth > w)
            {
                iWidth = w - StartX;
            }

            if (StartY + iHeight > h)
            {
                iHeight = h - StartY;
            }

            try
            {
                Bitmap bmpOut = new Bitmap(iWidth, iHeight, PixelFormat.Format24bppRgb);

                Graphics g = Graphics.FromImage(bmpOut);
                g.DrawImage(b, new Rectangle(0, 0, iWidth, iHeight), new Rectangle(StartX, StartY, iWidth, iHeight), GraphicsUnit.Pixel);
                g.Dispose();

                return bmpOut;
            }
            catch
            {
                return null;
            }
        }

        public static void AdjustPhoto(int toWidth, int toHeight, string filePath, string fromFileName, string toFileName, int maxWidth, int maxHeight)
        {
            Image originalImage = Image.FromFile(filePath + "/" + fromFileName);
            //如果尺寸不够返回保存原图
            if (originalImage.Width < toWidth && originalImage.Height < toHeight)
            {
                originalImage.Save(filePath + "/" + toFileName);
                originalImage.Dispose();
                return;
            }
            //根据图片大小获取新图片从原图片截取的区域
            int x, y, w, h;
            if (toHeight > 0)
            {
                if (toWidth > 0)
                {
                    if (originalImage.Width > toWidth && originalImage.Height > toHeight)
                    {
                        w = toWidth;
                        h = toWidth * originalImage.Height / originalImage.Width;
                        if (h > toHeight)
                        {
                            h = toHeight;
                            w = toHeight * originalImage.Width / originalImage.Height;
                            x = (toWidth - w) / 2;
                            y = 0;
                        }
                        else
                        {
                            x = 0;
                            y = (toHeight - h) / 2;
                        }
                    }
                    else if (originalImage.Width > toWidth)
                    {
                        w = toWidth;
                        h = toWidth * originalImage.Height / originalImage.Width;
                        x = 0;
                        y = (toHeight - h) / 2;
                    }
                    else if (originalImage.Height > toHeight)
                    {
                        h = toHeight;
                        w = toHeight * originalImage.Width / originalImage.Height;
                        x = (toWidth - w) / 2;
                        y = 0;
                    }
                    else
                    {
                        w = originalImage.Width;
                        h = originalImage.Height;
                        x = (toWidth - w) / 2;
                        y = (toHeight - h) / 2;
                    }
                }
                else
                {
                    if (originalImage.Height > maxHeight)
                    {
                        toWidth = toHeight * originalImage.Width / originalImage.Height;
                        x = 0;
                        y = 0;
                        w = toWidth;
                        h = toHeight;
                    }
                    else
                    {
                        x = 0;
                        y = 0;
                        w = originalImage.Width;
                        h = originalImage.Height;
                        toWidth = originalImage.Width;
                        toHeight = originalImage.Height;
                    }
                }
            }
            else
            {
                if (originalImage.Width > maxWidth)
                {
                    toHeight = toWidth * originalImage.Height / originalImage.Width;
                    x = 0;
                    y = 0;
                    w = toWidth;
                    h = toHeight;
                }
                else
                {
                    x = 0;
                    y = 0;
                    w = originalImage.Width;
                    h = originalImage.Height;
                    toWidth = originalImage.Width;
                    toHeight = originalImage.Height;
                }
            }
            Bitmap bm = new Bitmap(toWidth, toHeight);
            Graphics g = Graphics.FromImage(bm);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.Clear(Color.White);
            g.DrawImage(originalImage, new Rectangle(x, y, w, h), 0, 0, originalImage.Width, originalImage.Height, GraphicsUnit.Pixel);
            long[] quality = new long[1];
            quality[0] = 80;
            EncoderParameters encoderParams = new EncoderParameters();
            EncoderParameter encoderParam = new EncoderParameter(Encoder.Quality, quality);
            encoderParams.Param[0] = encoderParam;
            ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();//获得包含有关内置图像编码解码器的信息的ImageCodecInfo 对象。
            ImageCodecInfo jpegICI = null;
            for (int i = 0; i < arrayICI.Length; i++)
            {
                if (arrayICI[i].FormatDescription.Equals("JPEG"))
                {
                    jpegICI = arrayICI[i];//设置JPEG编码
                    break;
                }
            }
            if (jpegICI != null)
            {
                //bm.Save(Server.MapPath(path + "/thumb_" + filename), jpegICI, encoderParams);
                bm.Save(filePath + "/" + toFileName, jpegICI, encoderParams);
            }
            bm.Dispose();
            originalImage.Dispose();
            g.Dispose();
        }
        /// <summary>
        /// 保持比例图像缩放简易算法
        /// </summary>
        /// <param name="spcWidth"></param>
        /// <param name="spcHeight"></param>
        /// <param name="orgWidth"></param>
        /// <param name="orgHeight"></param>
        /// <returns></returns>
        public static void AdjustSize(int spcWidth, int spcHeight, int orgWidth, int orgHeight, out int finalWidth, out int finalHight)
        {
            Dictionary<string, int> size = new Dictionary<string, int>();
            // 原始宽高在指定宽高范围内，不作任何处理  
            if (orgWidth <= spcWidth && orgHeight <= spcHeight)
            {
                size["Width"] = orgWidth;
                size["Height"] = orgHeight;
            }
            else
            {
                // 取得比例系数  
                float w = orgWidth / (float)spcWidth;
                float h = orgHeight / (float)spcHeight;
                // 宽度比大于高度比  
                if (w > h)
                {
                    size["Width"] = spcWidth;
                    size["Height"] = (int)(w >= 1 ? Math.Round(orgHeight / w) : Math.Round(orgHeight * w));
                }
                // 宽度比小于高度比  
                else if (w < h)
                {
                    size["Height"] = spcHeight;
                    size["Width"] = (int)(h >= 1 ? Math.Round(orgWidth / h) : Math.Round(orgWidth * h));
                }
                // 宽度比等于高度比  
                else
                {
                    size["Width"] = spcWidth;
                    size["Height"] = spcHeight;
                }
            }
            finalWidth = size["Width"];
            finalHight = size["Height"];
        }

    }
}
