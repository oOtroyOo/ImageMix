﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageMix
{
   
    public class Mixer : IDisposable
    {
        public Image Image1;
        public Image Image2;
        public ColorType colorType = ColorType.black;
        private event Action<Image> done;
        private Image newImage = null;

        private List<Image> tempImages = new List<Image>();

        public enum ColorType
        {
            black = 0,
            color = 1
        }

        enum ColorPram
        {
            r,
            g,
            b
        }

        public void DoImage(Action<Image> action = null)
        {
            done = action;

            makeImage();
        }

        void makeImage()
        {


            MakeImage();


            if (done != null)
            {
                done(newImage);
            }
        }

        void MakeImage()
        {
            Bitmap bitmap1 = new Bitmap(Image1);
            tempImages.Add(bitmap1);
            Bitmap bitmap2 = new Bitmap(Image2);
            tempImages.Add(bitmap2);
            AdjustImage(bitmap1, bitmap2, out bitmap1, out bitmap2);
            tempImages.Add(bitmap1);
            tempImages.Add(bitmap2);
            Bitmap bitmap3 = new Bitmap(Math.Min(bitmap1.Width, bitmap2.Width), Math.Min(bitmap1.Height, bitmap2.Height));
            tempImages.Add(bitmap3);
            for (int x = Math.Abs(bitmap1.Width - bitmap2.Width) / 2; x < bitmap1.Width && x < bitmap2.Width; x++)
            {
                for (int y = Math.Abs(bitmap1.Height - bitmap2.Height) / 2; y < bitmap1.Height && y < bitmap2.Height; y++)
                {
                    Color gray;
                    Color current;
                    int baseAplha = 0;
                    if ((x + y) % 2 == 0)
                    {
                        current = bitmap1.GetPixel(x, y);
                        baseAplha = 255;
                    }
                    else
                    {
                        current = bitmap2.GetPixel(x, y);
                        baseAplha = 0;
                    }
                    if (colorType == ColorType.black)
                    {
                        gray = DoColorGray(current, baseAplha);
                    }
                    else
                    {
                        //gray = DoColorWhite(current, baseAplha);
                        gray = DoColorWhite(bitmap1.GetPixel(x, y), bitmap2.GetPixel(x, y));
                    }

                    bitmap3.SetPixel(x, y, gray);
                }
            }
            newImage = bitmap3;
        }



        Color DoColorGray(Color color, int baseAlpha)
        {
            int gray = GrayNum(color);
            //int alpha = AlphaFrom(gray, baseAlpha);
            //Color newColor = Color.FromArgb(alpha, gray, gray, gray);
            int alpha = AlphaFrom(gray, baseAlpha);
            Color newColor = Color.FromArgb(alpha, baseAlpha, baseAlpha, baseAlpha);
            return newColor;

        }

        private Color DoColorWhite(Color color1, Color color2)
        {
            // pa :
            // gray + alpha = 1           
            //p1-白底 p2 -黑底
            //p1接近白色，且p2接近黑色，aplha约低
            //p1接近黑色，且p2接近白色，aplha约高 
            // 183  203 132

            //
            int gray1 = GrayNum(color1);
            int gray2 = GrayNum(color2);
            int alpha1 = 255 - gray1;
            int alpha2 = gray2;
            Color color = Color.FromArgb(
                (alpha1 + alpha2) / 2,
                (color1.R + color2.R) / 2,
                (color1.G + color2.G) / 2,
                (color1.B + color2.B) / 2
                );
            return color;
        }
        void AdjustImage(Bitmap bitmap1, Bitmap bitmap2, out Bitmap bitmapa, out Bitmap bitmapb)
        {

            int width = Math.Min(bitmap1.Width, bitmap2.Width);
            int hight = 0;
            ImgHelper.AdjustSize(width, int.MaxValue, Image1.Width, Image2.Height, out width, out hight);
            bitmapa = ImgHelper.KiResizeImage(bitmap1, width, hight, 0);

            ImgHelper.AdjustSize(width, int.MaxValue, Image2.Width, Image2.Height, out width, out hight);
            bitmapb = ImgHelper.KiResizeImage(bitmap2, width, hight, 0);

        }

        int GrayNum(Color color)
        {
            return (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
        }
        int AlphaFrom(int gray, int baseAplha)
        {
            return 255 - Math.Abs(gray - baseAplha);
        }
        int GrayFrom(int color, ColorPram p)
        {
            switch (p)
            {
                case ColorPram.r:
                    return (int)(color * 0.3);
                case ColorPram.g:
                    return (int)(color * 0.59);
                case ColorPram.b:
                    return (int)(color * 0.11);
            }
            return 0;
        }

        int Clamp(int num, int a)
        {
            int b = a - num;
            if (b < 0)
            {
                b = 0;
            }
            return b;
        }

        void MakeImageColor()
        {
        }

        public void Dispose()
        {
            foreach (Image tempImage in tempImages)
            {
                try
                {
                    tempImage.Dispose();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            tempImages.Clear();
        }
    }
}
