﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageMix
{
    static class Program
    {
        public static Form1 from1;
        public static Form2 form2;
        public static Mixer mixer = new Mixer();
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            from1 = new Form1();
            form2 = new Form2();
            Application.Run(from1);
        }

        public static void DialogToText(FileDialog dialog, TextBox textBox)
        {
            if (dialog.CheckFileExists)
            {
                textBox.Text = dialog.FileName;
            }
        }
        public static string DialogToText(FileDialog dialog)
        {
            if (dialog.CheckFileExists)
            {
                return dialog.FileName;
            }
            return "";
        }
    }
}
